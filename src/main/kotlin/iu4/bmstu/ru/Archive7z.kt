package iu4.bmstu.ru




class Archive7z(fileBytes: ByteArray){

    class SignatureHeader(fileBytes: ByteArray){
        class Version(fileBytes: ByteArray){
            val Major = fileBytes[6]
            val Minor = fileBytes[7]
        }

        class StartHeader(fileBytes: ByteArray){
            val nextHeaderOffset: Long = decoder.ByteArrayToLong(fileBytes.copyOfRange(12, 20)) // UINT64
            val nextHeaderSize = decoder.ByteArrayToLong(fileBytes.copyOfRange(20, 28)) // UINT64
            val nextHeaderCRC = decoder.ByteArrayToInt(fileBytes.copyOfRange(28, 32))   // UINT32
        }

        val kSignature = fileBytes.copyOfRange(0,6) // BYTE kSignature[6] = {'7', 'z', 0xBC, 0xAF, 0x27, 0x1C};
        val version = Version(fileBytes)
        val startHeaderCRC = decoder.ByteArrayToInt(fileBytes.copyOfRange(8, 12))
        val startHeader = StartHeader(fileBytes)
    }

    class HeaderStructure(fileBytes: ByteArray) {
        class ArchiveProperty(){
            val propertyType: Byte = 0
            val propertySize: Long = 0
            val propertyData: ByteArray = ByteArray(0)
        }

        class ArchiveProperties(fileBytes: ByteArray){
            val kArchiveProperties = 0x02 // kArchiveProperties (0x02)
            var archiveProperties = ArrayList<ArchiveProperty>()
        }

        class StreamInfo(){
            class PackInfo(){
                val kPackInfo = 0x06
                val packPos: Long = 0 // UINT64
                val numPacksStreams: Long = 0 // UINT64
                val kSize = 0x09
                val packSizes: LongArray = LongArray(numPacksStreams.toInt())    // UINT64
                val kCRC = 0x0A
                val packCRCs: LongArray = LongArray(numPacksStreams.toInt())    // UINT64
            }
            class CodersInfo(){
                class Folder(){
                    class Coder(){
                        val folderFlag: Byte = 0
                        // 0:3 CodecIDSize
                        val codecIdSize = 0
                        val codecID: ByteArray = kotlin.ByteArray(codecIdSize)
                        // 4:  Is Complex Coder
                        val numInStreams: Long = 0
                        val numOutStreams: Long = 0
                        // 5:  There Are Attributes
                        val propertiesSize: Long = 0
                        val properties: ByteArray = kotlin.ByteArray(propertiesSize.toInt())
                        // 6:  Reserved
                        // 7:  There are more alternative methods. (Not used anymore, must be 0).
                    }

                    class BindPairs(){
                        val inIndex: Long = 0
                        val outIndex: Long = 0
                    }

                    val numCoders: Long = 0
                    val coders = ArrayList<Coder>()
                    val numBindPairs = 0    // NumBindPairs = NumOutStreamsTotal - 1;
                    val numPackedStreams = 0    // NumPackedStreams = NumInStreamsTotal - NumBindPairs;
                    val numPackedStreamsIndex = kotlin.LongArray(numPackedStreams)
                }

                val kUnPackInfo = 0x07

                val kFolder = 0x0B
                val numFolders: Long = 0
                val external: Byte = 0x0
                // 0
                val folders = ArrayList<Folder>()
                // 1
                val dataStreamIndex: Long = 0

                val kCodersUnPackSize: Byte = 0x0C
                // unPackSize for each Coder in each Folder
                val unPackSize = ArrayList<LongArray>()

                val kCRC: Byte = 0x0A
                val unPackCRCs = ByteArray(numFolders.toInt())

                val kEND = 0x00
            }
            class SubStreamsInfo(numFolders: Long){
                val kSubStreamsInfo = 0x08
                val kNumUnPackStream = 0x0D
                val NumUnPackStreamsInFolders = LongArray(numFolders.toInt())

                val kSize = 0x09
                val unPackSizes: MutableList<Long> = mutableListOf()

                val kCRC = 0x0A
                val CRCs: ByteArray = kotlin.ByteArray(0)
                val kEND = 0x00
            }
            val packInfo = PackInfo()
            val codersInfo = CodersInfo()
            val subStreamsInfo = SubStreamsInfo(codersInfo.numFolders)
            val kEND = 0x00
        }

        class AdditionalStreams(fileBytes: ByteArray){
            val kAdditionalStreamsInfo = 0x03
            val streamsInfo = ArrayList<StreamInfo>()
        }

        class MainStreams(fileBytes: ByteArray){
            val kMainStreamsInfo = 0x04
            val streamsInfo = ArrayList<StreamInfo>()
        }

        class FilesInfo(fileBytes: ByteArray){
        }
        val kHeader = 0x01    // kHeader (0x01)
        val archiveProperties = ArchiveProperties(fileBytes)
        val additionalStreams = AdditionalStreams(fileBytes)
        val mainStreams = MainStreams(fileBytes)
        val filesInfo = FilesInfo(fileBytes)
        val kEND = 0x00
    }

    val signatureHeader = SignatureHeader(fileBytes)
    val packedStreams: List<ByteArray> = ArrayList<ByteArray>()
    val headerBytes = fileBytes.copyOfRange(32 + signatureHeader.startHeader.nextHeaderOffset.toInt(), fileBytes.size)
    val headerStructure = HeaderStructure(fileBytes)

    init {
        // Header
        if (headerBytes[0].toInt() == headerStructure.kHeader){   // It is header
            when (headerBytes[1].toInt()){
                headerStructure.archiveProperties.kArchiveProperties ->
                    headerStructure.archiveProperties.archiveProperties
            }
        }
        else
        {
            // Error
            println("Error while parsing 7z header")
        }
    }
}