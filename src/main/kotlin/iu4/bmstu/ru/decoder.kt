package iu4.bmstu.ru
import java.io.File

class decoder
{
    companion object {
        fun ByteArrayToInt(bytes: ByteArray): Int{
            var sum = 0
            bytes.forEachIndexed { i, byte -> sum += byte.toInt().shl(i * 8) }
            return sum
        }
        fun ByteArrayToLong(bytes: ByteArray): Long{
            var sum: Long = 0
            bytes.forEachIndexed { i, byte -> sum += byte.toInt().shl(i * 8) }
            return sum
        }

        fun parce_7z(path: String){
            val filename = path
            println("Input file name = $filename")
            val file = File(filename)
            val fileBytes = file.readBytes()

            val archive = Archive7z(fileBytes)

            /*var mainHeaderArchived = false
            if (mainHeader[0].toInt() == 0x1)
                 mainHeaderArchived = false
            else if (mainHeader[0].toInt() == 0x17)
                mainHeaderArchived = true
            else {
                println("Error while parse 7z file")
                return
            }

            // Pack info
            if ((mainHeader[2].toInt() != 6) || (mainHeader[5].toInt() != 9)) {
                println("Error while parse 7z file")
                return
            }
            val mainHeaderThreadsStart = mainHeader[3].toInt()
            val mainHeaderThreadsCount = mainHeader[4].toInt()
            val decoderArchiveSize = mainHeader[6].toInt()

            // Coders Info
            if ((mainHeader[8].toInt() != 7) || (mainHeader[9].toInt() != 11)) {
                println("Error while parse 7z file")
                return
            }*/

        }

        @JvmStatic fun main(args: Array<String>) {
            if(args.isEmpty()) {
                println("Not enough arguments!")
                System.exit(-1)
            }
            parce_7z(args[0])
        }
    }
}